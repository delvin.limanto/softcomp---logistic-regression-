﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Tugas_1_2
{
    public partial class Form1 : Form
    {
        List<double> age = new List<double>();
        List<double> operationYear = new List<double>();
        List<double> positive = new List<double>();
        List<double> survivalStatus = new List<double>();

        List<double> hTeta = new List<double>();
        List<double> teta0 = new List<double>();
        List<double> teta1 = new List<double>();
        List<double> teta2 = new List<double>();
        List<double> teta3 = new List<double>();

        List<double> hTetaMinY = new List<double>();
        double totalHTetaMinY = 0.0;
        double totalHTetaMinYX1 = 0.0;
        double totalHTetaMinYX2 = 0.0;
        double totalHTetaMinYX3 = 0.0;

        double learningRate = 0.0001;

        public Form1()
        {
            InitializeComponent();
        }

        /**
         * Procedure to read file
         * @author Budi, Delvin, Gio, Ricky
         * 
         * @param string filename   nama file atau path file yang ingin dibaca
         */
        private void read_file(string filename)
        {
            var lines = File.ReadLines(filename);
            foreach (var line in lines)
            {
                string[] temp = line.Split(',');
                age.Add(Convert.ToDouble(temp[0]));
                operationYear.Add(Convert.ToDouble(temp[1]));
                positive.Add(Convert.ToDouble(temp[2]));
                survivalStatus.Add(Convert.ToDouble(temp[3]));

                //Set variabel awal
                hTeta.Add(0.0);
                teta0.Add(0.0);
                teta1.Add(0.0);
                teta2.Add(0.0);
                teta3.Add(0.0);
            }
        }

        /**
         * Procedure that run when training menu strip is clicked
         * @author Budi, Delvin, Gio, Ricky
         * 
         */
        private void trainingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            read_file("haberman.data");
            normalization(age);
            normalization(operationYear);
            normalization(positive);
            //for (int i = 0; i < age.Count; i++) listBox1.Items.Add(age[i] + " - " + operationYear[i] + " - " + positive[i]);
            logistic_regression();
        }

        /**
         * Procedure that do normalization
         * @author Budi, Delvin, Gio, Ricky
         * 
         * @param List<double> list    list yang ingin dinormalisasi
         */
        private void normalization(List<double> list)
        {
            double max = list.Max();
            for(int i = 0;i < list.Count; i++) list[i] = list[i] / max * 1.0;
        }

        /**
         * Procedure that count logistic regression
         * @author Budi, Delvin, Gio, Ricky
         * 
         */
        private void logistic_regression()
        {
            bool isKonvergen = false;
            while (!isKonvergen)
            {
                hTetaMinY.Clear();
                totalHTetaMinY = 0.0;
                totalHTetaMinYX1 = 0.0;
                totalHTetaMinYX2 = 0.0;
                totalHTetaMinYX3 = 0.0;

                count_hTetaMinusY();
                totalHTetaMinYX1 = count_hTetaMinusYVar(age);
                totalHTetaMinYX2 = count_hTetaMinusYVar(operationYear);
                totalHTetaMinYX3 = count_hTetaMinusYVar(positive);



                isKonvergen = true;
            }
        }

        /**
        * Procedure that count hTeta minus Y
        * @author Budi, Delvin, Gio, Ricky
        * 
        */
        private void count_hTetaMinusY()
        {
            for (int i = 0; i < survivalStatus.Count; i++)
            {
                double tmp = (hTeta[i] - survivalStatus[i]);
                totalHTetaMinY += tmp;
                hTetaMinY.Add(tmp);
            }
        }

        /**
       * Function that count hTeta minus Y multiple variable
       * @author Budi, Delvin, Gio, Ricky
       * 
       * @param List<double>    list    Variabel 
       * @return double
       */
        private double count_hTetaMinusYVar(List<double> list)
        {
            double total = 0.0;
            for(int i = 0;i < hTetaMinY.Count; i++) total += hTetaMinY[i] * list[i] * 1.0;
            return total;
        }


    }
}
